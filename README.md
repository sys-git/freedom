# README #

A python release management tool.

[ ![Codeship Status for sys-git/freedom](https://codeship.com/projects/7abb60f0-7d41-0132-fcd7-6602ec740206/status?branch=master)](https://codeship.com/projects/56601)
[![Build Status](https://api.shippable.com/projects/54b500d45ab6cc135288711c/badge?branchName=master)](https://app.shippable.com/projects/54b500d45ab6cc135288711c/builds/latest)
[![Documentation Status](https://readthedocs.org/projects/freedoms/badge/?version=latest&style=flat)](https://readthedocs.org/projects/freedoms/?badge=latest)

[![Downloads](https://pypip.in/download/freedom/badge.svg)](https://pypi.python.org/pypi/freedom/)
[![Latest Version](https://pypip.in/version/freedom/badge.svg)](https://pypi.python.org/pypi/freedom/)
[![Supported Python versions](https://pypip.in/py_versions/freedom/badge.svg)](https://pypi.python.org/pypi/freedom/)
[![Supported Python implementations](https://pypip.in/implementation/freedom/badge.svg)](https://pypi.python.org/pypi/freedom/)
[![Development Status](https://pypip.in/status/freedom/badge.svg)](https://pypi.python.org/pypi/freedom/)
[![Wheel Status](https://pypip.in/wheel/freedom/badge.svg)](https://pypi.python.org/pypi/freedom/)
[![Egg Status](https://pypip.in/egg/freedom/badge.svg)](https://pypi.python.org/pypi/freedom/)
[![Download format](https://pypip.in/format/freedom/badge.svg)](https://pypi.python.org/pypi/freedom/)
[![License](https://pypip.in/license/freedom/badge.svg)](https://pypi.python.org/pypi/freedom/)

### How do I get set up? ###

* **python setup.py install**
* Run the tests from source **python setup.py nosetests**
* Dependencies:  None
* How to run tests with all nosetests options set:  **./runtests.sh**
* Deployment instructions (coming soon):  **pip install freedom**



### Requirements ###

At least Python 2.6
Tested on Python 2.7, 3.4.0

### Contribution guidelines ###

I accept pull requests.

### Who do I talk to? ###

* Francis Horsman:  **francis.horsman@gmail.com**

### How do I run it from a shell ? ###

In the next release you will be able to do this in your shell from any location:

```
freedom-release <args>
```

### How do I run it programmatically ? ###

```
>>> python release.py --help

Usage: release.py [OPTIONS]

  argument parsing for the cmdline, scripts, programmatic entry-points.

  :param kwargs: kwargs exported by the click module.

Options:
  --verbose                       Enabled more verbose output.
  --wizard                        Run the release process as an interactive
                                  wizard.
  --tag-release                   tag this release
  --push-release                  push this release to git
  --build-release                 build this release
  --build-docs                    build this release' docs
  --publish-pypi                  publish release to pypi
  --publish-docs-pypi             publish release docs to pypi
  --publish-docs-rtd              publish release docs to ReadTheDocs
  --dry-run                       Perform a dry-run (no side effects in git or
                                  pypi)
  --register-project              Register project on pypi - see: --project-
                                  name
  --interactive / --unattended    Interactive or unattended execution
                                  (defaults to unattended)
  --git-propagate-env / --no-git-propagate-env
                                  Propagate the current environement when
                                  calling git (defaults to True)
  --python-propagate-env / --no-python-propagate-env
                                  Propagate the current environement when
                                  calling python (defaults to True)
  --interactive-prompt TEXT       Interactive prompt to display(defaults to
                                  >>>).
  --pypirc TEXT                   Path to .pypirc file.
  --generate-rcfile TEXT          Export all options to this file and exit
                                  immediately.
  --profile-env TEXT              Environmental variable which holds profile
                                  information in, defaults to
                                  RELEASEME_PROFILE.
  --profile TEXT                  Load options from this profile file
                                  (overrides all other options).
  --history-version TEXT          New version (optional - previous version
                                  will be incremented).
  --history-date TEXT             The date to use in the history synopsis when
                                  tagging a release.
  --history-comment TEXT          The changelog comment to add.
  --history-format TEXT           The changelog format to add (must contain
                                  three "%s" for version, date, comment).
  --history-file TEXT             The history text file to use.
  --history-delimiter TEXT        The history file synopsis delimiter (just
                                  prior to history entries).
  --history-meta TEXT             The history file synopsis (prior to the
                                  delimiter).
  --git-profile TEXT              Load options from this git profile file
                                  (overrides all other options)
  --git-repo TEXT                 Location of the git repo to release
                                  (defaults to cwd)
  --gitrc TEXT                    Location of the .gitconfig file (defaults to
                                  cwd)
  --project-name TEXT             Name of this project (same as that which
                                  would be published to pypi
  --help                          Show this message and exit.

```

