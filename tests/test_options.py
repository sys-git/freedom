# -*- coding: utf-8 -*-

import sys
import unittest

from freedom.options import get_options
from freedom.utils import compare_dicts


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.opts = '--dry-run ' \
                    '--interactive ' \
                    '--tag-release ' \
                    '--push-release ' \
                    '--build-release ' \
                    '--build-docs ' \
                    '--publish-pypi ' \
                    '--publish-docs-pypi ' \
                    '--publish-docs-rtd ' \
                    '--register-project ' \
                    '--pypirc=~/tmp/testbed/freedom/.pypirc ' \
                    '--profile=~/tmp/testbed/.releaserc ' \
                    '--history-file=meta/HISTORY.rst ' \
                    '--log-level=debug ' \
                    '--verbose ' \
                    '--log-mode=w ' \
                    '--git-profile=~/tmp/testbed/freedom/.gitrc.yaml ' \
                    '--git-repo=~/tmp/testbed/freedom/src ' \
                    '--gitrc=~/tmp/testbed/freedom/.gitconfig ' \
                    '--project-name=voo ' \
                    '--generate-rcfile=~/tmp/testbed/freedom/.releaserc '

    def tearDown(self):
        pass

    def test(self):
        sys.argv = self.opts.split()
        options = get_options(standalone_mode=False)
        expected_values = {'profile': u'~/tmp/testbed/.releaserc',
                           'build_docs': True,
                           'verbose': True,
                           'publish_docs_rtd': True,
                           'register_project': True,
                           'history_version': u'',
                           'pypirc': u'~/tmp/testbed/freedom/.pypirc',
                           'git_propagate_env': True,
                           'log_level': u'debug',
                           'history_comment': u'Latest freedom released '
                                              u'package.',
                           'dry_run': False,
                           'wizard': False,
                           'generate_rcfile': u'~/tmp/testbed/freedom'
                                              u'/.releaserc',
                           'publish_docs_pypi': True,
                           'python_propagate_env': True,
                           'git_repo': u'~/tmp/testbed/freedom/src',
                           'publish_pypi': True,
                           'project_name': u'voo',
                           'history_format': u'%s %s\n----------------\n* %s\n',
                           'history_file': u'meta/HISTORY.rst',
                           'history_delimiter': u'=========',
                           'gitrc': u'~/tmp/testbed/freedom/.gitconfig',
                           'build_release': True,
                           'history_date': u'2015-02-03',
                           'profile_env': u'RELEASEME_PROFILE',
                           'history_meta': u'.. :changelog:\n\nChangelog\n'
                                           u'=========\n\n',
                           'log_mode': u'w',
                           'git_profile': u'~/tmp/testbed/freedom/.gitrc.yaml',
                           'push_release': True,
                           'tag_release': True,
                           'interactive_prompt': u'>>>',
                           'interactive': True}
        assert compare_dicts(options, expected_values)


if __name__ == '__main__':  # pragma no cover
    unittest.main()
