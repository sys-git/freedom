
*************
Freedom
*************
.. currentmodule:: freedom

Usage
=====

Very much a Work In Progress

Quick start
-----------
.. code-block:: python

>>> python release.py --help

Usage: release.py [OPTIONS]

  argument parsing for the cmdline, scripts, programmatic entry-points.

  :param kwargs: kwargs exported by the click module.

    Options:
      --verbose                       Enabled more verbose output.
      --wizard                        Run the release process as an interactive
                                      wizard.
      --tag-release                   tag this release
      --push-release                  push this release to git
      --build-release                 build this release
      --build-docs                    build this release' docs
      --publish-pypi                  publish release to pypi
      --publish-docs-pypi             publish release docs to pypi
      --publish-docs-rtd              publish release docs to ReadTheDocs
      --dry-run                       Perform a dry-run (no side effects in git or
                                      pypi)
      --register-project              Register project on pypi - see: --project-
                                      name
      --interactive / --unattended    Interactive or unattended execution
                                      (defaults to unattended)
      --git-propagate-env / --no-git-propagate-env
                                      Propagate the current environement when
                                      calling git (defaults to True)
      --python-propagate-env / --no-python-propagate-env
                                      Propagate the current environement when
                                      calling python (defaults to True)
      --interactive-prompt TEXT       Interactive prompt to display(defaults to
                                      >>>).
      --pypirc TEXT                   Path to .pypirc file.
      --generate-rcfile TEXT          Export all options to this file and exit
                                      immediately.
      --profile-env TEXT              Environmental variable which holds profile
                                      information in, defaults to
                                      RELEASEME_PROFILE.
      --profile TEXT                  Load options from this profile file
                                      (overrides all other options).
      --history-version TEXT          New version (optional - previous version
                                      will be incremented).
      --history-date TEXT             The date to use in the history synopsis when
                                      tagging a release.
      --history-comment TEXT          The changelog comment to add.
      --history-format TEXT           The changelog format to add (must contain
                                      three "%s" for version, date, comment).
      --history-file TEXT             The history text file to use.
      --history-delimiter TEXT        The history file synopsis delimiter (just
                                      prior to history entries).
      --history-meta TEXT             The history file synopsis (prior to the
                                      delimiter).
      --git-profile TEXT              Load options from this git profile file
                                      (overrides all other options)
      --git-repo TEXT                 Location of the git repo to release
                                      (defaults to cwd)
      --gitrc TEXT                    Location of the .gitconfig file (defaults to
                                      cwd)
      --project-name TEXT             Name of this project (same as that which
                                      would be published to pypi
      --help                          Show this message and exit.

TODO: How the release process works.


.. _resourceLimits-conf:

Configuration
=============
The following freedom configuration values are honored by
:class:`freedom`. If the corresponding configuration is passed in to the
:class:`freedom` constructor, those will take precedence.

.. tabularcolumns:: |p{6.5cm}|p{8.5cm}|

============================== ================================================
``verbose``                    A more verbose output.
============================== ================================================


.. _resourceLimits-string:

.. _keyfunc-customization:

Recipes
=======

.. _logging:

Logging
-------
Each :class:`freedom` instance has a ``logger`` instance variable that is by
default **not** configured with a handler. You can add your own handler to obtain
log messages emitted by :mod:`freedom`.

Simple stdout handler::

    free = freedom(*args, **kwargs)
    free.logger.addHandler(StreamHandler())

Reusing all the handlers of the ``logger`` instance of the :class:`freedom` app::

    free = freedom(*args, **kwargs)
    for handler in free.logger.handlers:
        free.logger.addHandler(handler)

API
===

Core
----
.. automodule:: freedom.api
   :members:

.. automodule:: freedom.core
   :members:


Misc
----
.. automodule:: freedom.actions
   :members:
.. automodule:: freedom.apps
   :members:
.. automodule:: freedom.builder
   :members:
.. automodule:: freedom.cli
   :members:
.. automodule:: freedom.defaults
   :members:
.. automodule:: freedom.execute
   :members:
.. automodule:: freedom.export
   :members:
.. automodule:: freedom.handlers
   :members:
.. automodule:: freedom.history
   :members:
.. automodule:: freedom.monitor
   :members:
.. automodule:: freedom.options
   :members:
.. automodule:: freedom.profiles
   :members:
.. automodule:: freedom.publish
   :members:
.. automodule:: freedom.task
   :members:
.. automodule:: freedom.utils
   :members:
.. automodule:: freedom.vcs
   :members:


Command line
------------
.. automodule:: release
   :members: main


Exceptions
----------
.. automodule:: freedom.errors
   :members:

.. include:: ../../meta/HISTORY.rst

.. include:: ../../meta/LICENSE.rst

Requirements
============

.. include:: ../../requirements.txt

References
==========

.. include:: ../../meta/CONTRIBUTIONS.rst
