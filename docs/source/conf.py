# -*- coding: utf-8 -*-
#

import sys
import os

sys.path.insert(0, os.path.abspath('../../'))
sys.path.append(os.path.abspath('_themes'))

import freedom

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode',
]

templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
project = u'Freedom'
copyright = freedom.__copyright__

version = release = freedom.__version__
exclude_patterns = []
pygments_style = 'sphinx'
html_theme_options = {
    "index_logo": "logo.jpg"
}
html_theme_path = ["_themes"]
html_theme = 'flask'
html_static_path = ['_static']
html_style = 'limiter.css'

htmlhelp_basename = 'Freedomlimitdoc'
html_logo = 'logo.jpg'
html_favicon = 'icon.jpg'
html_sidebars = {
    'index': ['sidebarintro.html', 'localtoc.html', 'sourcelink.html',
              'searchbox.html'],
    '**': ['localtoc.html', 'relations.html',
           'sourcelink.html', 'searchbox.html']
}

latex_documents = [
    ('index', 'Freedom.tex', u'Freedom Documentation',
     u'Francis Horsman', 'manual'),
]
man_pages = [
    ('index', 'freedom', u'Freedom Documentation',
     [u'Francis Horsman'], 1)
]

texinfo_documents = [
    ('index', 'freedom', u'Freedom Documentation',
     u'Francis Horsman', 'Freedom', 'A python release management tool.',
     'Miscellaneous'),
]

intersphinx_mapping = {'python': ('http://docs.python.org/', None)
    , 'freedom': ('http://freedom.readthedocs.org/en/latest/', None)
}

autodoc_default_flags = [
    "members"
    , "show-inheritance"
]
