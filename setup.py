#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

if sys.version_info < (2, 6):
    sys.exit('Pyhton version 2.6 is required at a minimum.')

import os

try:
    from setuptools import setup, find_packages
except ImportError:
    import ez_setup

    ez_setup.use_setuptools()
    from setuptools import setup, find_packages

# Allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

import freedom
import versioneer

product = 'freedom'
meta_path = 'meta'

versioneer.VCS = 'git'
versioneer.versionfile_source = '%s/version.py' % product
versioneer.versionfile_build = '%s/version.py' % product
versioneer.tag_prefix = ''
versioneer.parentdir_prefix = '%s-' % product

history_path = history = os.path.join(meta_path, 'HISTORY.rst')
readme_path = 'README.md'
licence_path = os.path.join(meta_path, 'LICENCE')
classifiers_path = os.path.join(meta_path, 'CLASSIFIERS')
requirements_path = 'requirements.txt'

try:
    open(history_path).read()
except IOError:
    open(history_path, 'w').write('')

setup(
    name=product,
    packages=find_packages(exclude=('tests',)),
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    url=freedom.__url__,
    author=freedom.__author__,
    author_email=freedom.__email__,
    description=freedom.__short_description__,
    long_description=open(readme_path).read() + open(history_path).read(),
    license=open(licence_path).readlines()[1].strip(),
    platforms=['any'],
    classifiers=[k for k in
                 open(classifiers_path).read().split('\n')
                 if k],
    install_requires=[k for k in open(requirements_path).readlines() if k],
    zip_safe=False,
    keywords='Python release management',
    scripts=['bin/freedom-release'],
)
