# -*- coding: utf-8 -*-

"""
A simple helper interface to the more complex releaser.

This is the programmatic entry-point if calling from another script but does NOT
verify kwargs.
"""

from logging import getLogger
import copy

from .cli import cli
from .utils import update_dict


api_logger = getLogger('api')


def _exec(run, logger=api_logger, actions=None, handlers=None, **kwargs):
    new_kwargs = update_dict(copy.deepcopy(kwargs),
                             actions) if actions is not None else kwargs
    return cli(run=run, handlers=handlers, logger=logger, **new_kwargs)()


def main(run=False, logger_name=None, handlers=None, **kwargs):
    """
    Everything - Publish this release to pypi (possibly including the docs)

    :param run: Immediately run the release, False - return an object on which
        can be called with no arguments.
    :param logger_name: logger name to use.
    :param kwargs: kwargs to pass to the releaser constructor.
    """
    return _exec(run, logger_name, handlers, **kwargs)


def publish(run=False, logger_name=None, handlers=None, **kwargs):
    """
    Publish this release to pypi (possibly including the docs)

    :param run: Immediately run the release, False - return an object on which
        can be called with no arguments.
    :param logger_name: logger name to use.
    :param kwargs: kwargs to pass to the releaser constructor.
    """
    return _exec(run, logger_name, handlers,
                 dict(tag=True, push=True, publish=True), **kwargs)


def release(run=False, logger_name=None, handlers=None, **kwargs):
    """
    The full-monty: everything involved in a release.

    :param run: Immediately run the release, False - return an object on which
        can be called with no arguments.
    :param logger_name: logger name to use.
    :param kwargs: kwargs to pass to the releaser constructor.
    """
    return _exec(run, logger_name, handlers,
                 actions=dict(tag=True, push=True, publish=True), **kwargs)


def push(run=False, logger_name=None, handlers=None, **kwargs):
    """
    Convenience  method to Push the current code to git.

    FIXME: The exec dict is incorrect/need-updating.
    """
    return _exec(run, logger_name, handlers,
                 actions=dict(tag=False, push=True, publish=False, build=False),
                 **kwargs)


def tag(run=False, logger_name=None, handlers=None, **kwargs):
    """
    Tag the current release in git (auto-incrementing the version).

    FIXME: The exec dict is incorrect/need-updating.
    """
    return _exec(run, logger_name, handlers,
                 actions=dict(tag=True, push=False, publish=False, build=False),
                 **kwargs)


def build(run=False, logger_name=None, handlers=None, **kwargs):
    """
    Convenience  method to Build the current release.

    FIXME: The exec dict is incorrect/need-updating.
    """
    return _exec(run, logger_name, handlers,
                 actions=dict(tag=False, push=False, publish=False, build=True),
                 **kwargs)


if __name__ == '__main__':  # pragma no cover
    # Example using the api programmatically:
    def func(*args, **kwargs):
        print 'got event'

    kwargs = dict()  # Default settings.
    cli = build(handlers=func, **kwargs)
    cli()  # execute process.
    # or:
    cli = build(**kwargs)
    cli(handlers=func)  # execute process.


