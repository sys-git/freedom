# -*- coding: utf-8 -*-
"""
:summary: Sensible project defaults

:attention: 'options' flags must have their default value set to 'False'.
:author: francis.horsman@gmail.com
"""

import os


class ExportFormat(object):
    JSON = 'json'
    PICKLE = 'pickle'
    YAML = 'yaml'
    MSGPACK = 'msgpack'


DEFAULT_CWD = os.getcwd()
DEFAULT_INTERACTIVE = False
DEFAULT_INTERACTIVE_PROMPT = '>>>'
DEFAULT_PROJECT_NAME = 'my project'
DEFAULT_WIZARD = False
DEFAULT_BUILD_TEST = False
DEFAULT_EXPORT_OPTIONS = None
DEFAULT_EXPORT_FORMAT = ExportFormat.YAML
DEFAULT_DRYRUN = False
DEFAULT_LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
DEFAULT_LOG_FILE = '.log'
DEFAULT_LOG_LEVEL = 'debug'
DEFAULT_LOG_MODE = 'a'
DEFAULT_VERBOSE = False
DEFAULT_ENV = 'RELEASEME_PROFILE'
DEFAULT_TAG_RELEASE = False
DEFAULT_BUILD_RELEASE = False
DEFAULT_BUILD_RELEASE_DOCS = False
DEFAULT_PUBLISH_PYPI = False
DEFAULT_PUBLISH_DOCS_PYPI = False
DEFAULT_PUBLISH_DOCS_RTD = False
DEFAULT_PUSH_RELEASE = False
DEFAULT_REGISTER_PROJECT_PYPI = False
DEFAULT_PROFILE = '~/.releaserc'
DEFAULT_PYPIRC = os.path.join(os.path.realpath(os.environ.get('HOME', '~')),
                              '.pypirc')
DEFAULT_GITCONFIG = os.path.join(os.path.realpath(os.environ.get('HOME', '~')),
                                 '.gitconfig')
DEFAULT_GIT_PROFILE = '~/.gitrc'
DEFAULT_GIT_REPO = DEFAULT_CWD
DEFAULT_GITRC = DEFAULT_CWD
DEFAULT_GIT_PROPAGATE_ENV = True
DEFAULT_PYTHON_PROPAGATE_ENV = True
DEFAULT_HISTORY_FILE = 'HISTORY.rst'
DEFAULT_HISTORY_COMMENT = 'Latest freedom released package.'
DEFAULT_HISTORY_DATE = None
DEFAULT_HISTORY_VERSION = None
DEFAULT_HISTORY_DELIMITER = '========='
DEFAULT_HISTORY_META = """.. :changelog:

Changelog
%s

""" % DEFAULT_HISTORY_DELIMITER

DEFAULT_HISTORY_FORMAT = """%s %s
----------------
* %s
"""


class OptionsKey(object):
    PROJECT_NAME = 'project-name'
    VERBOSE = 'verbose'
    WIZARD = 'wizard'
    BUILD_TEST = 'test'
    DRY_RUN = 'dry-run'
    INTERACTIVE = 'interactive'
    INTERACTIVE_PROMPT = 'interactive-prompt'
    UNATTENDED = 'unattended'
    PYPIRC = 'pypirc'
    GENERATE_RCFILE = 'generate-rcfile'
    TAG_RELEASE = 'tag-release'
    PUSH_RELEASE = 'push-release'
    BUILD_RELEASE = 'build-release'
    BUILD_RELEASE_DOCS = 'build-docs'
    PUBLISH_RELEASE_PYPI = 'publish-pypi'
    PUBLISH_RELEASE_DOCS_PYPI = 'publish-docs-pypi'
    PUBLISH_RELEASE_DOCS_RTD = 'publish-docs-rtd'
    PROFILE_ENV = 'profile-env'
    PROFILE = 'profile'
    REGISTER_PROJECT_PYPI = 'register-project'
    HISTORY_VERSION = 'history-version'
    HISTORY_DATE = 'history-date'
    HISTORY_COMMENT = 'history-comment'
    HISTORY_FORMAT = 'history-format'
    HISTORY_FILE = 'history-file'
    HISTORY_DELIMITER = 'history-delimiter'
    HISTORY_META = 'history-meta'
    GIT_REPO = 'git-repo'
    GIT_PROFILE = 'git-profile'
    GITRC = 'gitrc'
    GIT_PROPAGATE_ENV = 'git-propagate-env'
    GIT_NO_PROPAGATE_ENV = '-'.join(['no', GIT_PROPAGATE_ENV])
    PYTHON_PROPAGATE_ENV = 'python-propagate-env'
    PYTHON_NO_PROPAGATE_ENV = '-'.join(['no', PYTHON_PROPAGATE_ENV])
    LOG_FILE = 'log-file'
    LOG_LEVEL = 'log-level'
    LOG_MODE = 'log-mode'


class OptionsKeyHelp(object):
    LOG_LEVEL = 'Logging level, one of: \'debug\', \'info\', \'warn\', ' \
                '\'error\', \'exception\' (defaults to %s).' % DEFAULT_LOG_LEVEL
    LOG_MODE = 'Logging mode, one of: \'a\' (append), \'w\' (overwrite) (' \
               'defaults to %s).' % DEFAULT_LOG_MODE


OPTIONS_YES = ['Y', 'y']
OPTIONS_NO = ['N', 'n']


def get_opts_key(key):
    return key.replace('-', '_')


def get_opt(key):
    return ''.join(['--', key])


if __name__ == '__main__':  # pragma no cover
    pass
