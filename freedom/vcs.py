# -*- coding: utf-8 -*-
"""
:summary: All vcs operations are done through here.

:author: francis.horsman@gmail.com
"""

# from git import *
#
#
# class Git(object):
# """
# This class performs all git command handling.
# """
#
# def __init__(self, **kwargs):
# try:
# path = kwargs['git_repo']
# except:
# raise ValueError('git repo path not specified!')
# self.repo = Repo(path)
# self.config_writer = self.repo.config_writer()
#
# def add(self, push_all=False, comment=None):
# s = ['push_release']
# if push_all is True:
# s.append('-a')
# if comment is not None:
# s.append('-m')
# s.append(comment)
# return self.git(*tuple(s))
#
# def tag(self, name, comment=None):
# if comment is not None:
# return self.repo.tag(name, comment)
# return self.repo.tag(name)
#
# def push_release(self, push_all=False, push_tags=False):
# if push_all and push_tags:
# raise ValueError('push_all and push_tags are mutually exclusive !')
# if push_all:
# print('Pushing ALL to git')
# self.git('push_release', '--all')
# if push_tags:
# print('Pushing TAGS to git')
# self.git('push_release', '--tags')
# if not push_all and not push_tags:
# self.git('push_release')

from .helper import Helper


class Git(Helper):
    NAME = 'vcs'

    def __init__(self, core, logger=None, **kwargs):
        assert core.executor
        Helper.__init__(self, core, logger=logger, **kwargs)

        if not self.profile.vcs.git_repo:
            # Should be taken care of in the args parsing already!
            raise ValueError('git repo path not specified!')

        # Now check the status of the repo (fail-fast):
        self.status()

    def status(self):
        return self.executor.git('status')

    @property
    def tags(self):
        """
        Get the list of tags from the current repo.
        :return: list[str(tag)]
        """
        return self.executor.git('tag') or []

    @property
    def current_tag(self):
        return self.tags[-1:]

    @staticmethod
    def _make_comment(comment):
        return ''.join(['\'', comment, '\''])

    def add(self, add_all=False, comment=None):
        s = ['add']
        if add_all is True:
            s.append('-a')
        if comment is not None:
            s.append('-m')
            s.append(self._make_comment(comment))
        return self.executor.git(*tuple(s))

    def tag(self, tag_name, comment=None):
        s = ['tag', tag_name]
        if comment is not None:
            s.append('-m')
            s.append(self._make_comment(comment))
        return self.executor.git(*tuple(s))

    def commit(self, commit_all=True, comment=None):
        s = ['commit']
        if commit_all is True:
            s.append('-a')
        if comment is not None:
            s.append('-m')
            s.append(self._make_comment(comment))
        return self.executor.git(*tuple(s))

    def push(self, push_all=False, push_tags=False):
        if push_all:
            self._push(True, False)
        if push_tags:
            self._push(False, True)

    def _push(self, push_all, push_tags):
        s = ['push']
        if push_all and push_tags:
            raise Exception('push_all and push_tags are mutually exclusive')
        elif push_all:
            self.logger.info('Pushing ALL to git')
            s.append('--all')
        elif push_tags:
            self.logger.info('Pushing TAGS to git')
            s.append('--tags')
        else:
            self.logger.info('Pushing to git')
        return self.executor.git(*tuple(s))

    def __str__(self):
        return 'GitRepo(%s)' % self.profile.vcs.git_repo


if __name__ == '__main__':  # pragma no cover
    pass
