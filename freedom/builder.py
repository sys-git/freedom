# -*- coding: utf-8 -*-
"""
:summary: All building related activities in here.

:author: francis.horsman@gpypimail.com
"""

from .helper import Helper


class Builder(Helper):
    NAME = 'builder'

    def __init__(self, core, logger=None, **kwargs):
        assert core.executor
        assert core.monitor
        Helper.__init__(self, core, logger=logger, **kwargs)

    def _build_release(self, version):
        self.logger.info('Building: %s' % version)
        return self.executor.python('setup.py', 'build_release', 'sdist',
                                    'bdist_egg')

    def _build_and_upload_release(self, version):
        self.logger.info('Building and uploading: %s' % version)
        return self.executor.python('setup.py', 'build_release', 'sdist',
                                    'bdist_egg', 'upload')

    def _upload_release(self, version):
        self.logger.info('Building and uploading: %s' % version)
        return self.executor.python('setup.py', 'upload')

    def build_release(self, version, upload=False):
        return self._build_and_upload_release(
            version) if upload else self._build_release(
            version)

    def build_test(self, *args, **kwargs):
        return self.executor.shell('runtests', *args, **kwargs)

    def build_release_docs(self):
        self.logger.info('Building release docs')
        return self.executor.python('setup.py', 'build_sphinx')

    def publish_release_docs_pypi(self):
        self.logger.info('Publishing release docs to pypi')
        return self.executor.python('setup.py', 'upload_sphinx')

    def build_and_publish_release_docs_pypi(self):
        self.build_release_docs()
        self.publish_release_docs_pypi()


if __name__ == '__main__':  # pragma no cover
    pass
