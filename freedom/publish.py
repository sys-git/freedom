# -*- coding: utf-8 -*-
"""
:summary: All publishing to pypi is done from here.

:author: francis.horsman@gmail.com
"""

from .helper import Helper


class Publisher(Helper):
    NAME = 'publisher'

    def __init__(self, core, logger=None, **kwargs):
        assert core.builder
        assert core.executor
        assert core.monitor
        Helper.__init__(self, core, logger=logger, **kwargs)

    def pypi(self, new_version):
        self.logger.info('Publishing %s to pypi' % new_version)
        if not self.profile.misc.dry_run:
            return self.builder.build_release(new_version, upload=True)
        else:
            self.logger.info('simulated publish_release.')

    def register_pypi(self, name=None):
        name = name if name is not None else self.profile.misc.project_name
        self.logger.info('Registering project %s on pypi' % name)
        env = self.builder.get_env(home=self.profile.build.pypirc)
        return self.executor.python('setup.py', 'register', '-r', 'pypi',
                                    env=env)


if __name__ == '__main__':  # pragma no cover
    pass
