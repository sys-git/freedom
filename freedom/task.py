# -*- coding: utf-8 -*-
"""
:summary: All task related activity in here.

:author: francis.horsman@gmail.com
"""

import collections
from abc import ABCMeta
from functools import wraps
import traceback
from types import NoneType

from six import add_metaclass, string_types, integer_types

from .errors import TaskConfigurationError, InteractiveAbort, TaskError
from .helper import Helper


class _TaskError(Exception):
    pass


class _TaskContext(dict):
    """
    Holds details of 'stuff' for passing between tasks.
    """
    pass


class TaskNotRequired(Exception):
    def __init__(self, about):
        Exception.__init__(self, about)
        self.about = about


_SUPPORTED_TASKS = collections.defaultdict(list)


class TaskState(object):
    NOT_RUN = 'not run'
    RUNNING = 'running'
    RUNNING_WAITING_ON_INPUT = 'running but waiting for user input'
    FINISHED_OK = 'finished ok'
    FINISHED_ERROR = 'finished with error'
    FINISHED_FAILURE = 'finished with failure'


class _TaskStatus(object):
    def __init__(self, task, state=TaskState.NOT_RUN, result=None):
        self._task = task
        self._state = state
        self._result = result

    def __str__(self):
        s = ['status:']
        state = self.state
        s.append(state if state else 'unknown')
        if state in [TaskState.FINISHED_FAILURE, TaskState.FINISHED_ERROR]:
            s.append(': %s' % self.result)
        return ' '.join(s)

    @property
    def result(self):
        return self._result

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        self._state = state


def _create_about_str(task, about=None):
    status_str = '' if not hasattr(task, 'status') else ' - %s' % str(
        task.status)
    about_str = task.ABOUT if not about else about
    return ''.join(['<Task(', about_str, ')', status_str, '>'])


class TaskRegistry(type):
    def __new__(mcs, name, bases, dct):
        if name != '_Task':
            action = dct.get('ACTION', None)
            profile = dct.get('PROFILE', None)
            phase = dct.get('PHASE', None)
            about = dct.get('ABOUT', None)
            if not bases == (object,):
                for what, attribute, e_type in [
                    (action, 'ACTION', string_types),
                    (profile, 'PROFILE', string_types),
                    (phase, 'PHASE', integer_types),
                    (about, 'ABOUT', string_types),
                ]:
                    if not what or not isinstance(what, e_type):
                        raise TaskConfigurationError(
                            '%s is not configured correctly, it must specify a'
                            '%s class attribute of type %s' % (
                                what, attribute, e_type))

            def build_action(act):
                @wraps(build_action)
                def __inner(self, context, **kwargs):
                    func = getattr(self._parent.actions, act)
                    return func(context, **kwargs)

                return __inner

            def build_str(abt):
                @wraps(build_str)
                def __inner(self):
                    return _create_about_str(self, abt)

                return __inner

            dct['_action'] = build_action(action)
            dct['__str__'] = build_str(about)
            del dct['PHASE']
            del dct['ACTION']
        cls = super(TaskRegistry, mcs).__new__(mcs, name, bases, dct)
        if name != '_Task':
            _SUPPORTED_TASKS[phase].append(cls)
        return cls


@add_metaclass(TaskRegistry)
@add_metaclass(ABCMeta)
class _Task(object):
    """
    Holds details of an individual task (action).

    The exact mechanics of how tasks work and how they pass data between
        themselves is T.B.D.
    """

    def __init__(self, parent):
        self._parent = parent  # parent Task instance.
        self._check_is_required(parent.profile)
        self._status = _TaskStatus(self)

    @property
    def status(self):
        return self._status

    def _check_is_required(self, profile):
        what = profile
        for token in self.PROFILE.split('.'):
            what = getattr(what, token, None)
            if not what:
                raise TaskNotRequired(_create_about_str(self))

    @property
    def impl(self):
        return self._parent.actions

    def __call__(self, context):
        """
        Perform the action related to this task.

        :param context: dict - update in-place as you see fit.
        :return: The result from the action, T.B.D.
        """
        return self._action(context)


class _TaskBuildDocs(_Task):
    PHASE = 1
    ABOUT = 'Build-docs'
    ACTION = 'build_release_docs'
    PROFILE = 'build.build_docs'


class _TaskBuildTest(_Task):
    PHASE = 1
    ABOUT = 'Build-test'
    ACTION = 'build_test'
    PROFILE = 'build.build_test'


class _TaskBuildRelease(_Task):
    PHASE = 1
    ABOUT = 'Build-release'
    ACTION = 'build_release'
    PROFILE = 'build.build.build_release'


class _TaskTagRelease(_Task):
    PHASE = 2
    ABOUT = 'Vcs-tag-release'
    ACTION = 'tag_release'
    PROFILE = 'vcs.tag_release'


class _TaskPushRelease(_Task):
    PHASE = 2
    ABOUT = 'Vcs-Push-Release'
    ACTION = 'push_release'
    PROFILE = 'vcs.push_release'


class _TaskPublishPypi(_Task):
    PHASE = 3
    ACTION = 'publish_release'
    ABOUT = 'Publish-release-pypi'
    PROFILE = 'publish.publish_pypi'


class _TaskPublishDocsPypi(_Task):
    PHASE = 3
    ABOUT = 'Publish-docs-pypi'
    ACTION = 'publish_release_docs_pypi'
    PROFILE = 'publish.publish_docs_pypi'


class Task(Helper):
    NAME = 'task'

    def __init__(self, core, logger=None, **kwargs):
        assert core.monitor
        Helper.__init__(self, core, logger=logger, **kwargs)
        self._context = self._create_context
        self._tasks = self._init()
        self._current_task = None
        self._finished = True if not self._tasks else False

    @property
    def _create_context(self):
        return _TaskContext(version=None)

    @property
    def current_task(self):
        return self._current_task

    @current_task.setter
    def current_task(self, task):
        if not isinstance(task, (_Task, NoneType)):
            raise TaskError(task)
        self._current_task = task

    def _init(self):
        result = []
        for phase, tasks in sorted(_SUPPORTED_TASKS.items()):
            self.logger.debug('Building phase %d tasks' % phase)
            for task in tasks:
                try:
                    impl = task(self)
                except TaskNotRequired as t:
                    self.logger.debug('Task not required: %s' % t.about)
                else:
                    self.logger.debug(
                        'Task required: %s' % _create_about_str(impl))
                    result.append(impl)
        return result

    @property
    def finished(self):
        return self._finished is True

    def run(self):
        """
        Iterate within this run method so as to keep the state of the iteration
        within this class instance.
        """
        self.logger.warn('Starting task processing')
        try:
            for _ in self._flow:
                yield
        except InteractiveAbort:
            raise
        except Exception as err:
            # If one task fails, we fail completely:
            self.logger.error('\n'.join([err.message, traceback.format_exc()]))
            raise
        finally:
            self._finished = True
            self.logger.warn('Finished task processing.')

    @property
    def _flow(self):
        """
        Work all the tasks ( the order has already been determined during
            _init() ).
        Lets deal with the release flow first, then take care of docs, etc
            after.
        """
        for index, task in enumerate(self._tasks):
            self.current_task = task
            with self:
                self.logger.debug('Working task %d: %s' % (index, str(task)))
                yield task(self._context)

    def __enter__(self):
        self.current_task.status.state = TaskState.RUNNING

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not exc_type:
            self.current_task.status.state = TaskState.FINISHED_OK
        elif isinstance(exc_val, InteractiveAbort):
            self.current_task.status.state = TaskState.FINISHED_FAILURE
        else:
            self.current_task.status.state = TaskState.FINISHED_ERROR


if __name__ == '__main__':  # pragma no cover
    pass
