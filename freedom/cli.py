# -*- coding: utf-8 -*-
"""
:summary: The command-line invoked wrapper for the release system.

:author: francis.horsman@gmail.com
"""

from logging import getLogger

from six import string_types

from .core import releaser


class cli(object):
    """
    Command-line wrapper.
    """
    LOGGER_NAME = 'releaser-cli'

    def __init__(self, run=True, logger=None, handlers=None, **kwargs):
        """
        :param kwargs: see ..release.py
        """
        self.logger = getLogger(logger) if isinstance(logger, string_types) \
            else logger if logger is not None else getLogger(cli.LOGGER_NAME)
        self.kwargs = kwargs
        self.run = run
        self.handlers = handlers

    def __call__(self, **kwargs):
        return self.execute(**kwargs)

    def execute(self, run=None, logger=None, kwargs=None, handlers=None):
        run = run if run is not None else self.run
        logger = logger if logger is not None else self.logger
        kwargs = kwargs if kwargs is not None else self.kwargs
        handlers = handlers if handlers is not None else self.handlers

        r = releaser(logger=logger, handlers=handlers, **kwargs)
        return r() if r and run is True else r


if __name__ == '__main__':  # pragma no cover
    pass
