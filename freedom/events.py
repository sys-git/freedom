# -*- coding: utf-8 -*-
"""
:summary: The events sub-system.
Events can be listened for (ie: to update a GUI).

:author: francis.horsman@gmail.com
"""

import collections
from abc import abstractmethod

from six import string_types

from .errors import UnsupportedEvent, InvalidEventLevel
from .utils import EventLevel
from .helper import _SUPPORTED_HELPERS as HELPERS
from .helper import Helper


class Event(object):
    def __init__(self, name, level, data=None):
        self.name = name
        self.level = level
        self.data = data or dict()

    def __str__(self):
        data = str(self.data)  # [:200]  # Only display the first 200 chars.
        return 'Event_%s[%s] - %s' % (self.name, self.level, data)

    @staticmethod
    def build(name, level=EventLevel.DEFAULT, **kwargs):
        """
        Factory method to create an event from the given name.

        :param name: name of event to create.
        :param args: args to pass to the event's constructor.
        :param kwargs: kwargs to pass to the event's constructor.
        :return: An instance of Event.
        :raise UnsupportedEvent: Unsupported event.
        """
        if name not in HELPERS.keys():
            raise UnsupportedEvent(name)
        level = Event._check_level(level)
        return Event(name, level=level, data=kwargs)

    @staticmethod
    def _check_level(level):
        level = level.lower() if level and isinstance(level, string_types) \
            else ''
        if level not in EventLevel._levels:
            raise InvalidEventLevel(level)
        return level


class _Events(Helper):
    NAME = 'events'

    def __init__(self, events_func, core, logger=None, **kwargs):
        Helper.__init__(self, core, logger=logger, **kwargs)
        self._events_func = events_func
        self.events = collections.deque()

    def __getattribute__(self, name):
        if name in HELPERS.keys():
            # An event from a helper has been generated.
            def __inner(**kwargs):
                return self._event(Event.build(name, **kwargs))

            return __inner
        return object.__getattribute__(self, name)

    def event(self, evt):
        """
        Fire an event.

        :param evt: An event of type(Event).
        """
        if not isinstance(evt, Event):
            raise UnsupportedEvent(evt)
        return self._event(evt)

    @abstractmethod
    def _event(self, evt):
        """
        Handle event firing.

        :param evt: type(Event) the event that was fired.
        """
        raise NotImplementedError()


class _NoEventsHandler(_Events):
    NAME = 'events'

    def _event(self, evt):
        self.logger.debug('Event: %s' % str(evt))


class _EventsHandler(_Events):
    NAME = 'events'

    def _event(self, evt):
        self.logger.debug('Firing event: %s' % str(evt))
        try:
            self._events_func(evt)
        except Exception as err:
            self.logger.warn('Error in event handler: %s\n%s' % (evt, err))


def Events():
    def __inner(core, logger=None, **kwargs):
        """
        Factory method to create the Events Helper.

        :param events_func: None: no events sub-system. Non-None && callable: A
            function to be called on a given event. Note: this function is
            called
            after the current task has completed. This function is called in the
            thread of the tasks sub-system.
        :return: An instance of _Events as required.
        """
        events_func = core.handlers.events
        cls = _EventsHandler if callable(events_func) else _NoEventsHandler
        return cls(events_func, core, logger=logger, **kwargs)

    result = __inner
    result.NAME = 'events'
    return result


if __name__ == '__main__':  # pragma no cover
    pass
