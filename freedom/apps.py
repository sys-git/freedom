# -*- coding: utf-8 -*-
"""
:summary: All apps callable from either the bin/freedom-* scripts OR from the
command-line.

:author: Francis.horsman@gmail.com
"""

import sys

from .api import build as api_build
from .api import main as api_main
from .api import publish as api_publish
from .api import push as api_push
from .api import release as api_release
from .api import tag as api_tag
from .errors import Exit, InteractiveAbort, InteractiveDeny
from .options import get_options


def build(run=True, **kwargs):
    return api_build(run=run, **get_options(standalone_mode=False, **kwargs))


def tag(run=True, **kwargs):
    return api_tag(run=run, **get_options(standalone_mode=False, **kwargs))


def release(run=True, **kwargs):
    return api_release(run=run, **get_options(standalone_mode=False, **kwargs))


def push(run=True, **kwargs):
    return api_push(run=run, **get_options(standalone_mode=False, **kwargs))


def publish(run=True, **kwargs):
    return api_publish(run=run, **get_options(standalone_mode=False, **kwargs))


def main_entry(run=True, **kwargs):
    """
    Main entry point to the command-line app.

    :param kwargs: kwargs exported by the click module.
    """
    try:
        return api_main(run=run, **get_options(standalone_mode=False, **kwargs))
    except Exit:
        sys.exit(0)
    except (InteractiveAbort, InteractiveDeny) as err:
        sys.stderr.write(str(err) + '\n')
        sys.exit(-1)


if __name__ == '__main__':  # pragma no cover
    pass
