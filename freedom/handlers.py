# -*- coding: utf-8 -*-
"""
:summary: All caller's handlers's handling (!) goes in here.

:author: francis.horsman@gmail.com
"""

from .helper import Helper
from .utils import keyedDictionary


class Handlers(keyedDictionary, Helper):
    NAME = 'handlers'
    KEYS = ['events', 'user_input']

    def __init__(self, core, logger=None, handlers=None, **kwargs):
        assert core.monitor
        Helper.__init__(self, core, logger=logger, **kwargs)
        handlers = handlers or dict()
        handlers = {name: handlers.get(name, None) for name in Handlers.KEYS}
        keyedDictionary.__init__(self, handlers)


if __name__ == '__main__':  # pragma no cover
    pass
