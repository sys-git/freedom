# -*- coding: utf-8 -*-
"""
:summary: All exceptions we export.

:author: francis.horsman@gmail.com
"""


class Exit(Exception):
    pass


class ProfileLoadError(Exception):
    """
    There was an error loading the profile
    """

    def __init__(self, exc, msg):
        super(ProfileLoadError, self).__init__(exc)
        self.load_error = msg


class HelperAccessorError(Exception):
    """
    There was an runtime error accessing a helper from another helper.

    A helper was accessed by a helper before it was created by the core.
    """

    def __init__(self, helper_name, exc=None):
        super(HelperAccessorError, self).__init__(helper_name, exc)
        self.helper_name = helper_name


class NoProfileFile(Exception):
    """
    The profile file is not found at the given location.
    """
    pass


class UnknownExportCodec(Exception):
    """
    An export codec with the given name was not found.
    """

    def __init__(self, codec):
        Exception.__init__(self, codec)
        self.codec = codec

    def __str__(self):
        return 'Unknown exporter codec: \'%s\'' % self.codec


class ExporterConfigurationError(Exception):
    """
    An error occurred in the exporter codec creation.
    """
    pass


class TaskConfigurationError(Exception):
    """
    An error occurred in the task creation.
    """
    pass


class ProfileConfigurationError(Exception):
    """
    An error occurred in the profile creation.
    """
    pass


class HelperConfigurationError(Exception):
    """
    An error occurred in the Helper creation.
    """
    pass


class ImportFailure(Exception):
    """
    An error occurred when using the exporter to import data.
    """
    pass


class UnsupportedEvent(ValueError):
    """
    An attempt was made to raise an event that was not supported.
    """

    def __init__(self, evt):
        ValueError.__init__(self,
                            'An attempt was made to raise an event that was '
                            'not supported: %s' % str(evt))
        self.event = evt


class InvalidEventLevel(ValueError):
    """
    An attempt was made to raise an event with a level that was not supported.
    """

    def __init__(self, evt):
        ValueError.__init__(self,
                            'An attempt was made to raise an event that was'
                            'not supported: '
                            '%s' % str(evt))
        self.event = evt


class InteractiveMessage(Exception):
    def __init__(self, msg=None, response=None, releaser=None, additional=None):
        self.msg = msg
        self.response = response
        self.releaser = releaser
        self.additional = additional

    @property
    def command(self):
        return self.msg


class InteractiveAbort(InteractiveMessage):
    """
    An interactive user requested an abort.
    """

    def __str__(self):
        s = list()
        s.append('Interactive abort for command: %s' % self.msg)
        s.append('User response: %s' % self.response)
        try:
            s.append('During: %s' % str(self.releaser.task.current_task))
        except:
            pass
        if self.additional is not None:
            s.append(str(self.additional))
        return '\n'.join(s)


class InteractiveDeny(InteractiveMessage):
    """
    An interactive user requested a deny.
    """

    def __str__(self):
        s = list()
        s.append('Interactive deny for command: %s' % self.msg)
        s.append('User response: %s' % self.response)
        try:
            s.append('During task: %s' % str(self.releaser.task.current_task))
        except:
            pass
        return ',\n'.join(s)


class TaskError(TypeError):
    """
    A task was used that was not of type: [NoneType, _Task]
    """

    def __init__(self, item):
        TypeError.__init__(self, item)
        self.item = item

    def __str__(self):
        return 'TypeError: Invalid task type: %s' % type(self.item)


class ConfigurationError(ValueError):
    """
    A configuration value is incorrect.
    """

    def __init__(self, name, value, extended_info=None):
        self.name = name
        self.value = value
        self.extended_info = (' ' + extended_info) if extended_info else ''
        ValueError.__init__(self, 'Configuration setting \'%s\' value \'%s\' '
                                  'is incorrect%s' % (self.name, self.value,
                                                      self.extended_info))


if __name__ == '__main__':  # pragma no cover
    pass
