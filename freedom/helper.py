# -*- coding: utf-8 -*-
"""
:summary:

:author: francis.horsman@gmail.com
"""

from abc import ABCMeta

import six

from .errors import HelperConfigurationError


_SUPPORTED_HELPERS = dict()


class _HelperRegistry(type):
    def __new__(mcs, name, bases, dct):
        if name != 'Helper':
            attr_name = dct.get('NAME', None)
            if not bases == (object,) and not attr_name:
                raise HelperConfigurationError(
                    '%s is not configured correctly, it must specify a '
                    'NAME class attribute.' % name)

        cls = super(_HelperRegistry, mcs).__new__(mcs, name, bases, dct)
        if name != 'Helper':
            _SUPPORTED_HELPERS[attr_name] = cls
        return cls


@six.add_metaclass(_HelperRegistry)
@six.add_metaclass(ABCMeta)
class Helper(object):
    def __init__(self, core, logger=None, **kwargs):
        assert len(kwargs.keys()) == 0
        self.core = core
        self._logger = logger

    @property
    def profile(self):
        return self.core.profile

    @property
    def logger(self):
        return self._logger if self._logger is not None else self.core.logger

    def __getattribute__(self, name):
        if name in _SUPPORTED_HELPERS.keys():
            return getattr(self.core, name, None)
        return object.__getattribute__(self, name)


if __name__ == '__main__':  # pragma no cover
    pass
