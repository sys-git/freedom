# -*- coding: utf-8 -*-
"""
:summary: All utility functionality goes in here.

:attention: Serialisation to json is attempted using the following libraries in
    order: (ujson, simplejson, json)

:author: francis.horsman@gmail.com
"""

import copy
import os
from abc import abstractmethod, ABCMeta
import sys
from functools import wraps

import six






# try:
# unicode = unicode
# except NameError:
# # 'unicode' is undefined, must be Python 3
# str = str
# unicode = str
# bytes = bytes
#     basestring = (str, bytes)
# else:
#     # 'unicode' exists, must be Python 2
#     str = str
#     unicode = unicode
#     bytes = str
#     basestring = basestring

try:
    import ujson as json
except ImportError:
    try:
        import simplejson as json
    except ImportError:
        try:
            import json
        except ImportError:
            sys.exit('No json library available')


def update_dict(a, b, in_place=True):
    if not in_place:
        a = copy.deepcopy(a)
    for k, v in b.iteritems():
        a[k] = v
    return a


def new_env(d):
    env = os.environ.copy()
    env.update(d)
    return env


def get_env_str(env=None):
    env = env if env is not None else os.environ.copy()
    s = ', '.join(['(%s=%s)' % (k, v) for k, v in env.iteritems()])
    return s


@six.add_metaclass(ABCMeta)
class iExporter(object):
    @abstractmethod
    def build(self, d):
        raise NotImplementedError

    @abstractmethod
    def create(self, d):
        raise NotImplementedError

    @abstractmethod
    def dump(self):
        raise NotImplementedError


class EventLevel(object):
    OK = 'ok'
    ERROR = 'error'
    FATAL = 'fatal'
    DEFAULT = OK
    _levels = []


def enumerate_event_levels():
    EventLevel._levels = [getattr(EventLevel, i) for i in dir(EventLevel) if
                          i[0].isupper()]


enumerate_event_levels()


class keyedDictionary(dict):
    def __getattribute__(self, item):
        try:
            return self[item]
        except KeyError:
            return object.__getattribute__(self, item)

    def __setitem__(self, key, value):
        if key not in self.KEYS:
            raise KeyError('disallowed key (%s) - only keys allowed are: %s' % (
                key, self.KEYS))
        return dict.__setitem__(self, key, value)


@six.add_metaclass(ABCMeta)
class EventData(keyedDictionary):
    pass


class EventCreatorEvent(EventData):
    KEYS = ['action', 'err', 'extra']

    def __init__(self, action=None, err=None,
                 extra=None):
        EventData.__init__(self, dict(action=action, err=err, extra=extra))


def event_creator(where, level_fail, level_ok, action, extra=None):
    @wraps(event_creator)
    def _outer(target_func):
        @wraps(target_func)
        def __inner(self, *args, **kwargs):
            data = EventCreatorEvent(
                **dict(extra=(extra or dict()),
                       action=action,
                       err=None))
            try:
                result = target_func(self, *args, **kwargs)
            except Exception as err:
                data['err'] = err
                getattr(self.events, where)(level=level_fail, **data)
                raise
            else:
                getattr(self.events, where)(level=level_ok, **data)
                return result

        return __inner

    return _outer


def compare_dicts(a, b):
    if not isinstance(a, dict) or not isinstance(b, dict):
        return False
    if a == b:
        return True
    if len(a.keys()) != len(b.keys()):
        return False
    if sorted(a.keys()) != sorted(b.keys()):
        return False
    for key in a.keys():
        if a[key] != b[key]:
            return False
    return True


if __name__ == '__main__':  # pragma no cover
    pass
