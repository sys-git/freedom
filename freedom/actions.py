# -*- coding: utf-8 -*-
"""
:summary: All actions related functionality goes in here.

:author: francis.horsman@gmail.com
"""

from .helper import Helper
from .options import OptionsKey
from .utils import EventLevel, event_creator


class Actions(Helper):
    """
    Actions are called by a Task only.

    :see: .task.py
    :attention: all public methods must have the same signature:
        "def method_name(self, context)".
    """
    NAME = 'actions'

    def __init__(self, core, logger=None, **kwargs):
        assert core.history
        assert core.publisher
        assert core.vcs
        assert core.builder
        assert core.monitor
        Helper.__init__(self, core, logger=logger, **kwargs)

    @event_creator(NAME, EventLevel.ERROR, EventLevel.OK,
                   OptionsKey.TAG_RELEASE)
    def tag_release(self, context):
        self.logger.info('Action: Tag-Release')
        history_version_overwrite = self.profile.history.history_file
        new_version = self.history.generate_new_version(
            history_version_overwrite, self.history.current_version)
        self.history.rewrite_history_from_profile(new_version)
        self.history.set_current_version(new_version)

    @event_creator(NAME, EventLevel.ERROR, EventLevel.OK,
                   OptionsKey.PUSH_RELEASE)
    def push_release(self, context):
        self.logger.info('Action: Push-Release')
        return self.vcs.push(push_all=True, push_tags=True)

    @event_creator(NAME, EventLevel.ERROR, EventLevel.OK,
                   OptionsKey.PUBLISH_RELEASE_PYPI)
    def publish_release(self, context):
        self.logger.info('Action: Publish-Release')
        version = context.get('version', None)
        if version is None:
            version = self.history.current_version
        return self.publisher.pypi(version)

    @event_creator(NAME, EventLevel.ERROR, EventLevel.OK,
                   OptionsKey.REGISTER_PROJECT_PYPI)
    def register_project(self, context):
        self.logger.info('Action: Register-Project')
        return self.publisher.register_pypi()

    @event_creator(NAME, EventLevel.ERROR, EventLevel.OK,
                   OptionsKey.BUILD_RELEASE)
    def build_release(self, context):
        self.logger.info('Action: Build-Release')
        version = context.get('version', None)
        if version is None:
            version = self.history.current_version
        return self.builder.build_release(version)

    @event_creator(NAME, EventLevel.ERROR, EventLevel.OK, OptionsKey.BUILD_TEST)
    def build_test(self, context):
        self.logger.info('Action: Build-Test')
        return self.builder.build_test()

    @event_creator(NAME, EventLevel.ERROR, EventLevel.OK,
                   OptionsKey.BUILD_RELEASE_DOCS)
    def build_release_docs(self, context):
        self.logger.info('Action: Build-Release-docs-pypi')
        return self.builder.build_release_docs()

    def build_and_publish_release_docs_pypi(self, context):
        self.logger.info('Action: Build-Publish-Release-docs-pypi')
        return self.builder.build_and_publish_release_docs_pypi()

    def build_and_publish_release_docs_rtd(self, context):
        self.logger.info('Action: Build-Publish-Release-docs-rtd')
        return self.builder.build_and_publish_release_docs_rtd()

    @event_creator(NAME, EventLevel.ERROR, EventLevel.OK,
                   OptionsKey.PUBLISH_RELEASE_DOCS_PYPI)
    def publish_release_docs_pypi(self, context):
        self.logger.info('Action: Publish-Release-docs-pypi')
        return self.builder.publish_release_docs_pypi()


if __name__ == '__main__':  # pragma no cover
    pass
