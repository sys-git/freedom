# -*- coding: utf-8 -*-
"""
:summary: All logging and event related functionality is done in here.

:author: francis.horsman@gmail.com
"""

import os
import sys
from logging import DEBUG, getLogger, StreamHandler, \
    Formatter, FileHandler
from logging import getLogger as Logger

from six import string_types

from .defaults import DEFAULT_LOG_FORMAT, OptionsKey
from .errors import ConfigurationError
from .helper import Helper


class Monitor(Helper):
    NAME = 'monitor'

    def __init__(self, core, logger=None):
        Helper.__init__(self, core, logger=logger)
        self._loggers = {}
        self._setup_logging()

    def _setup_logging(self, format=DEFAULT_LOG_FORMAT):
        desired_level = self.profile.monitor.log_level
        self._logging_level = desired_level.upper() if isinstance(
            desired_level, string_types) else desired_level

        root = getLogger()
        try:
            root.setLevel(self._logging_level)
        except ValueError:
            raise ConfigurationError(OptionsKey.LOG_LEVEL, self._logging_level)
        fmt = Formatter(format)

        # Set the file handler and default config
        filename = self.get_logger_filename()
        if filename:
            mode = self.profile.monitor.log_mode
            try:
                fh = FileHandler(filename, mode=mode)
            except ValueError:
                raise ConfigurationError(OptionsKey.LOG_MODE, mode, 'mode')
            fh.setLevel(self._logging_level)
            fh.setFormatter(fmt)
            root.addHandler(fh)

        if self.profile.monitor.verbose:
            # Emit ALL logging information to stdout:
            self._logging_level = DEBUG

            # Add a console logger in verbose mode also:
            ch = StreamHandler(sys.stdout)
            ch.setLevel(self._logging_level)
            ch.setFormatter(fmt)
            root.addHandler(ch)

    def get_logger_filename(self):
        return os.path.realpath(self.profile.monitor.log_file)

    def get_logger(self, name, *args, **kwargs):
        logger = Logger(name, *args, **kwargs)
        self._loggers[name] = logger
        return logger

    def get_logger_names(self):
        return self._loggers.keys()


if __name__ == '__main__':  # pragma no cover
    pass
