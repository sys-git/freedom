# -*- coding: utf-8 -*-
"""
:summary:

:author: Francis.horsman@gmail.com
"""

from datetime import datetime as dt

import click

from freedom.defaults import OptionsKeyHelp, DEFAULT_ENV, DEFAULT_PROFILE, \
    DEFAULT_HISTORY_FILE, DEFAULT_HISTORY_DELIMITER, DEFAULT_HISTORY_META, \
    DEFAULT_TAG_RELEASE, DEFAULT_BUILD_RELEASE, DEFAULT_PUBLISH_PYPI, \
    DEFAULT_GIT_PROFILE, get_opt, OptionsKey, DEFAULT_VERBOSE, \
    DEFAULT_DRYRUN, DEFAULT_PUBLISH_DOCS_PYPI, DEFAULT_PUBLISH_DOCS_RTD, \
    DEFAULT_BUILD_RELEASE_DOCS, DEFAULT_PUSH_RELEASE, DEFAULT_PYPIRC, \
    DEFAULT_EXPORT_OPTIONS, DEFAULT_GIT_REPO, DEFAULT_GITRC, \
    DEFAULT_LOG_LEVEL, DEFAULT_GIT_PROPAGATE_ENV, DEFAULT_HISTORY_COMMENT, \
    DEFAULT_PYTHON_PROPAGATE_ENV, DEFAULT_INTERACTIVE_PROMPT, \
    DEFAULT_WIZARD, DEFAULT_HISTORY_FORMAT, DEFAULT_INTERACTIVE, \
    DEFAULT_REGISTER_PROJECT_PYPI, DEFAULT_PROJECT_NAME, DEFAULT_LOG_MODE


@click.command()
# Options with no parameters:
@click.option(get_opt(OptionsKey.VERBOSE), is_flag=True,
              default=DEFAULT_VERBOSE,
              help='Enabled more verbose output.')
@click.option(get_opt(OptionsKey.WIZARD), is_flag=True,
              default=DEFAULT_WIZARD,
              help='Run the release process as an interactive wizard.')
@click.option(get_opt(OptionsKey.TAG_RELEASE), is_flag=True,
              default=DEFAULT_TAG_RELEASE,
              help='tag this release')
@click.option(get_opt(OptionsKey.PUSH_RELEASE), is_flag=True,
              default=DEFAULT_PUSH_RELEASE,
              help='push this release to git')
@click.option(get_opt(OptionsKey.BUILD_RELEASE), is_flag=True,
              default=DEFAULT_BUILD_RELEASE,
              help='build this release')
@click.option(get_opt(OptionsKey.BUILD_RELEASE_DOCS), is_flag=True,
              default=DEFAULT_BUILD_RELEASE_DOCS,
              help='build this release\' docs')
@click.option(get_opt(OptionsKey.PUBLISH_RELEASE_PYPI), is_flag=True,
              default=DEFAULT_PUBLISH_PYPI,
              help='publish release to pypi')
@click.option(get_opt(OptionsKey.PUBLISH_RELEASE_DOCS_PYPI), is_flag=True,
              default=DEFAULT_PUBLISH_DOCS_PYPI,
              help='publish release docs to pypi')
@click.option(get_opt(OptionsKey.PUBLISH_RELEASE_DOCS_RTD), is_flag=True,
              default=DEFAULT_PUBLISH_DOCS_RTD,
              help='publish release docs to ReadTheDocs')
@click.option(get_opt(OptionsKey.DRY_RUN), is_flag=True, default=DEFAULT_DRYRUN,
              help='Perform a dry-run (no side effects in git or pypi)')
@click.option(get_opt(OptionsKey.REGISTER_PROJECT_PYPI), is_flag=True,
              default=DEFAULT_REGISTER_PROJECT_PYPI,
              help='Register project on pypi - see: --%s' %
                   OptionsKey.PROJECT_NAME)
# Boolean options:
@click.option('/'.join([get_opt(OptionsKey.INTERACTIVE),
                        get_opt(OptionsKey.UNATTENDED)]),
              default=DEFAULT_INTERACTIVE,
              help='Interactive or unattended execution '
                   '(defaults to unattended)')
@click.option('/'.join([get_opt(OptionsKey.GIT_PROPAGATE_ENV),
                        get_opt(OptionsKey.GIT_NO_PROPAGATE_ENV)]),
              default=DEFAULT_GIT_PROPAGATE_ENV,
              help='Propagate the current environement when calling git'
                   ' (defaults to True)')
@click.option('/'.join([get_opt(OptionsKey.PYTHON_PROPAGATE_ENV),
                        get_opt(OptionsKey.PYTHON_NO_PROPAGATE_ENV)]),
              default=DEFAULT_PYTHON_PROPAGATE_ENV,
              help='Propagate the current environement when calling python'
                   ' (defaults to True)')
# Options with parameters:
@click.option(get_opt(OptionsKey.LOG_LEVEL),
              default=DEFAULT_LOG_LEVEL,
              help=OptionsKeyHelp.LOG_LEVEL)
@click.option(get_opt(OptionsKey.LOG_MODE),
              default=DEFAULT_LOG_MODE,
              help=OptionsKeyHelp.LOG_MODE)
@click.option(get_opt(OptionsKey.INTERACTIVE_PROMPT),
              default=DEFAULT_INTERACTIVE_PROMPT,
              help='Interactive prompt to display'
                   '(defaults to %s).' % DEFAULT_INTERACTIVE_PROMPT)
@click.option(get_opt(OptionsKey.PYPIRC), default=DEFAULT_PYPIRC,
              help='Path to .pypirc file.')
@click.option(get_opt(OptionsKey.GENERATE_RCFILE),
              default=DEFAULT_EXPORT_OPTIONS,
              help='Export all options to this file and exit immediately.')
@click.option(get_opt(OptionsKey.PROFILE_ENV), default=DEFAULT_ENV,
              help='Environmental variable which holds profile information in,'
                   ' defaults to %s.' % DEFAULT_ENV)
@click.option(get_opt(OptionsKey.PROFILE), default=DEFAULT_PROFILE,
              help='Load options from this profile file'
                   ' (overrides all other options).')
@click.option(get_opt(OptionsKey.HISTORY_VERSION), default='',
              help='New version'
                   ' (optional - previous version will be incremented).')
@click.option(get_opt(OptionsKey.HISTORY_DATE),
              default=dt.utcnow().strftime('%Y-%m-%d'),
              help='The date to use in the history synopsis when tagging a'
                   ' release.')
@click.option(get_opt(OptionsKey.HISTORY_COMMENT),
              default=DEFAULT_HISTORY_COMMENT,
              help='The changelog comment to add.')
@click.option(get_opt(OptionsKey.HISTORY_FORMAT),
              default=DEFAULT_HISTORY_FORMAT,
              help='The changelog format to add (must contain three "%s" for '
                   'version, date, comment).')
@click.option(get_opt(OptionsKey.HISTORY_FILE), default=DEFAULT_HISTORY_FILE,
              help='The history text file to use.')
@click.option(get_opt(OptionsKey.HISTORY_DELIMITER),
              default=DEFAULT_HISTORY_DELIMITER,
              help='The history file synopsis delimiter'
                   ' (just prior to history entries).')
@click.option(get_opt(OptionsKey.HISTORY_META), default=DEFAULT_HISTORY_META,
              help='The history file synopsis (prior to the delimiter).')
@click.option(get_opt(OptionsKey.GIT_PROFILE), default=DEFAULT_GIT_PROFILE,
              help='Load options from this git profile file'
                   ' (overrides all other options)')
@click.option(get_opt(OptionsKey.GIT_REPO), default=DEFAULT_GIT_REPO,
              help='Location of the git repo to release (defaults to cwd)')
@click.option(get_opt(OptionsKey.GITRC), default=DEFAULT_GITRC,
              help='Location of the .gitconfig file (defaults to cwd)')
@click.option(get_opt(OptionsKey.PROJECT_NAME), default=DEFAULT_PROJECT_NAME,
              help='Name of this project (same as that which would be '
                   'published to pypi')
def get_options(**kwargs):
    """
    argument parsing for the cmdline, scripts, programmatic entry-points.

    :param kwargs: kwargs exported by the click module.
    """
    return kwargs


if __name__ == '__main__':  # pragma no cover
    pass
