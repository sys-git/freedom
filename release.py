# -*- coding: utf-8 -*-

"""
CLI interface to the release management process
    (exposes the entire API).
"""

from freedom.apps import main_entry as main

if __name__ == '__main__':  # pragma no cover
    main()
